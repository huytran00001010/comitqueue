// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var teamSchema = new Schema({
    name: { type: String, required: true },
    password: { type: String, required: true },
    members: [],
    queue: [],
},
{ timestamps: true});

// the schema is useless so far
// we need to create a model using it
var Team = mongoose.model('Team', teamSchema);

// make this available to our users in our Node applications
module.exports = Team;