var express = require('express');
var router = express.Router();

const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Index', employees: employees});
});

router.get('/notFound', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../views/errors/', 'notFound.html'));
});


/* GET home page. */
router.get('/queue', function(req, res, next) {
  res.render('queue', { title: 'Express' });
});


module.exports = router;
