var express = require('express');
var router = express.Router();
const path = require('path');
// grab the model
var Team = require('../model/team');

router.get('/status*', function (req, res, next) {
    // console.log('team status')
    return res.sendFile(path.join(__dirname, '../views/team', 'status.html'))
});

router.get('/edit/:name', function (req, res, next) {
    return res.sendFile(path.join(__dirname, '../views/team', 'edit.html'))
});

router.get('/:name', function (req, res, next) {
    return res.sendFile(path.join(__dirname, '../views/team', 'index.html'))
});



module.exports = router;
