var express = require('express');
var router = express.Router();
const path = require('path');
// grab the model
var Team = require('../model/team');
/**
 * Create new team, default member is leader
 */
router.post('/', function (req, res, next) {
    // console.log(req.body)
    let name = req.body.name;
    let members = ['leader'];
    members = members.concat(req.body.members);
    let password = req.body.password;
    let email = req.body.email;
    let newTeam = Team({
        name: name + '_' + Date.now(),
        members: members,
        password: password,
        email: email
    })

    newTeam.save(function (err, team) {
        if (err) {
            // console.log(err)
            return res.status(500).send();
        }
        console.log(team)
        let teamDto = {
            name: team.name,
            members: team.members,
            createdAt: team.createdAt,
            updatedAt: team.updatedAt
        };
        return res.json(teamDto);
    });
});

/**
 * view all members of team
 */
router.get('/:name/members', function (req, res, next) {
    console.log('team index')
    let name = req.params.name;
    if (name) {
        Team.findOne({ name: name }, function (err, team) {
            if (err || !team) return res.status(404).send();
            // console.log(team)
            let teamDto = {
                name: team.name,
                members: team.members
            };
            return res.json(teamDto);
        });

    }
    else {
        return res.status(400).send();
    }

});

/**
 * view queue to commit of team
 */
router.get('/:name/queue', function (req, res, next) {
    console.log('team index')
    let name = req.params.name;
    let member = req.params.member;
    if (name) {
        Team.findOne({ name: name }, function (err, team) {
            if (err || !team) return res.status(400).send();
            console.log(team)
            let teamDto = {
                name: team.name,
                currentTurn: team.queue[0] ? team.queue[0] : null,
                queue: team.queue ? team.queue : null
            };
            return res.json(teamDto);
        });

    }
    else {
        return res.status(400).send();
    }

});

/**
 * member order to queue
 */
router.get('/:name/startturn/:member', function (req, res, next) {
    // console.log('team index')
    let name = req.params.name;
    let member = req.params.member;
    if (name) {
        Team.findOne({ name: name }, function (err, team) {
            console.error(err)
            console.error(team)
            if (err || !team) return res.status(404).send();
            console.log(team)
            /**
             * If queue is empty and last turn is not member and register turn less than 2
             */

            if (team.members.indexOf(member) > -1 && (team.queue.length === 0 || team.queue[team.queue.length - 1] !== member && countApear(member, team.queue) < 2)) {
                team.queue.push(member)

                team.save(function (err, team) {
                    console.log(team)
                    if (err) return res.status(500).send();
                    let teamDto = {
                        name: team.name,
                        currentTurn: team.queue[0] ? team.queue[0] : null,
                        queue: team.queue ? team.queue : null
                    };
                    return res.json(teamDto);
                })
            }
            else {
                let teamDto = {
                    name: team.name,
                    currentTurn: team.queue[0] ? team.queue[0] : null,
                    queue: team.queue ? team.queue : null
                };
                return res.json(teamDto);
            }
        });

    }
    else {
        return res.status(400).send();
    }

});

/**
 * current member end his turn
 */
router.get('/:name/endturn/:member', function (req, res, next) {
    let name = req.params.name;
    let member = req.params.member;
    if (name) {
        Team.findOne({ name: name }, function (err, team) {
            console.error(err)
            console.error(team)
            if (err || !team) return res.status(404).send();
            console.log(team)
            /**
             * remove member at top (current turn)
             */

            if (team.members.indexOf(member) > -1 && team.queue.length > 0 && member === team.queue[0]) {
                team.queue.splice(0, 1)

                team.save(function (err, team) {
                    console.log(team)
                    if (err) return res.status(500).send();
                    let teamDto = {
                        name: team.name,
                        currentTurn: team.queue[0] ? team.queue[0] : null,
                        queue: team.queue ? team.queue : null
                    };
                    return res.json(teamDto);
                })
            }
            else {
                let teamDto = {
                    name: team.name,
                    currentTurn: team.queue[0] ? team.queue[0] : null,
                    queue: team.queue ? team.queue : null
                };
                return res.json(teamDto);
            }
        });

    }
    else {
        return res.status(400).send();
    }

});

/**
 * Update team's member name
 */
router.post('/edit', function (req, res, next) {
    console.log(req.path)

    //TODO: need implement update team member with validate password
    let name = req.body.name;
    let members = ['leader'];
    members = members.concat(req.body.members);
    members = members.filter(function (n) { return n != undefined && n !== ''})
    let password = req.body.password;
    let email = req.body.email;
    let backURL = '/team/edit/' + name;

    if (name) {
        Team.findOne({ name: name }, function (err, team) {
            if (err || !team) return res.redirect(backURL);
            team.members = members;

            console.log(team)
            team.save(function () {
                return res.redirect(backURL);
            })
        })
    }
    else {
        return res.redirect(backURL);
    }




});

function countApear(member, countQueue) {
    if (countQueue instanceof Array)
        return countQueue.reduce(function (total, currentValue) {
            return currentValue === member ? total += 1 : total;
        }, 0)
    else return 0;
}

module.exports = router;
