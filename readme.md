## Commit Queue

I have worked for many development teams.
The commit and push code to repository that is really hard.
You need wait and notify to others befor you commit to avoid comflict code.

This code is working like semaphore. Each members need register and wait until my turn to comit code.
Each member only register 2 time in queue.

## How to run my code

* Clone this code to local folder , update list of your developers in `config\employees.json`
* Run `npm install & npm start` in your local folder
* Other partners can access `xxxx:9909` to register semaphore to comit code.

## Require

This is using socket IO for operation. Your network must enable `port 9909` and `socket protocal`