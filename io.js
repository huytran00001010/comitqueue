// const employees = require('./config/employees.json');
let queue = []

const io = require('socket.io')({
    path: '/queuehandler'
});

io.use(auth).on('connection', function (socket) {
    socket.emit('update_ui', { thisTurn: queue[0], allQueue: queue });

    socket.on('register_turn', function (data) {
        console.log(data);
        if (queue.length === 0 || queue[queue.length - 1]['userName'] !== data.userName && countApear(data.userName) < 2)
            queue.push({ userName: data.userName, timestamp: Date.now() })
        sendUpdate();
    });

    socket.on('end_turn', function (data) {
        console.log(data);

        if (socket.userName === queue[0]['userName'])
            queue.splice(0, 1)
        sendUpdate();
    });

    socket.on('disconnect', function (data) {
        console.log('disconnect user ' + socket.userName);
        // queue = queue.filter(function (a) { return a.userName !== socket.userName })
    })


});


function auth(socket, next) {
    if (socket.handshake.query && socket.handshake.query.userName) {
        // if (employees.includes(socket.handshake.query.userName)) {
        //     socket.userName = socket.handshake.query.userName;
        //     next();
        // }

    }
    next(new Error('Authentication error'));

}

function sendUpdate() {
    io.of('/').emit('update_ui', { thisTurn: queue[0], allQueue: queue });
}

function countApear(userName) {
    var countQueue = queue;
    return countQueue.reduce(function (total, currentValue) {
        return currentValue.userName === userName ? total += 1 : total;
    }, 0)
}


module.exports = io;